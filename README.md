[![Latest Stable Version](https://img.shields.io/badge/packgist-v1.0.3-blue)](https://packagist.org/packages/wangqiang/swoft-crud)
[![Php Version](https://img.shields.io/badge/php-%3E=7.1-brightgreen.svg?maxAge=2592000)](https://secure.php.net/)
[![Swoole Version](https://img.shields.io/badge/swoole-%3E=4.4.1-brightgreen.svg?maxAge=2592000)](https://github.com/swoole/swoole-src)

Swoft CRUD component

## Introduction

Swoft CRUD is a Devtool based on the Swoft framework. 


## Document

- [中文文档](https://www.kancloud.cn/wq_mack/swoft-crud)

## Discuss

- Email 2908755265@qq.com
- QQ 2908755265

## Requirement

- [PHP 7.1+](https://github.com/php/php-src/releases)
- [Swoole 4.3.4+](https://github.com/swoole/swoole-src/releases)
- [Composer](https://getcomposer.org/)
- [Swoft 2.0.0+](https://www.swoft.org/)

## Install

### Composer

```bash
composer require wangqiang/swoft-crud
```

## Start

- Create CRUD files

```bash
[root@swoft swoft]# php bin/swoft crud:gen -t admin
```

## License

Swoft CRUD is an open-source software licensed under the [LICENSE](LICENSE)
