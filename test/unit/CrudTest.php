<?php declare(strict_types=1);


namespace SwoftTest\Consul\Unit;


use Swoft\Crud\Model\Logic\EntityLogic;
use SwoftTest\Crud\Unit\TestCase;

/**
 * Class AgentTest
 *
 * @since 2.0
 */
class CrudTest extends TestCase
{

    /**
     */
    public function setUp()
    {

    }

    /**
     * @throws ClientException
     * @throws ServerException
     */
    public function testRegisterService()
    {
        $this->assertInstanceOf(EntityLogic::class,bean('crudEntityLogic'));
    }
}
