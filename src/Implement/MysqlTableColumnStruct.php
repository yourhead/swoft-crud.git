<?php declare(strict_types=1);


namespace Swoft\Crud\Implement;


use Swoft\Crud\Contract\MysqlTableColumn;
use Swoft\Bean\Annotation\Mapping\Bean;

/**
 * Class MysqlTableColumnStruct
 * @Bean(name="columnStruct",scope="prototype")
 */
class MysqlTableColumnStruct implements MysqlTableColumn
{
    /**
     * @var array $originData
     */
    private $originData;

    /**
     * @var string $columnName
     */
    private $columnName;

    /**
     * @var int $ordinalPosition
     */
    private $ordinalPosition;

    /**
     * @var mixed $columnDefault
     */
    private $columnDefault;

    /**
     * @var bool $isNullable
     */
    private $isNullable;

    /**
     * @var string $dataType
     */
    private $dataType;

    /**
     * @var int $characterMaximumLength
     */
    private $characterMaximumLength;

    /**
     * @var int $characterOctetLength
     */
    private $characterOctetLength;

    /**
     * @var int $numericPrecision
     */
    private $numericPrecision;

    /**
     * @var int $numericScale
     */
    private $numericScale;

    /**
     * @var int $datetimePrecision
     */
    private $datetimePrecision;

    /**
     * @var string $characterSetName
     */
    private $characterSetName;

    /**
     * @var string $collationName
     */
    private $collationName;

    /**
     * @var string $columnType
     */
    private $columnType;

    /**
     * @var string $columnKey
     */
    private $columnKey;

    /**
     * @var string $extra
     */
    private $extra;

    /**
     * @var string $columnComment
     */
    private $columnComment;

    /**
     * @param string $columnName
     */
    public function setColumnName(string $columnName): void
    {
        $this->columnName = $columnName;
    }

    /**
     * @param int $ordinalPosition
     */
    public function setOrdinalPosition(int $ordinalPosition): void
    {
        $this->ordinalPosition = $ordinalPosition;
    }

    /**
     * @param mixed $columnDefault
     */
    public function setColumnDefault($columnDefault): void
    {
        $this->columnDefault = $columnDefault;
    }

    /**
     * @param bool $isNullAble
     */
    public function setIsNullable(bool $isNullAble): void
    {
        $this->isNullable = $isNullAble;
    }

    /**
     * @param string $dataType
     */
    public function setDataType(string $dataType): void
    {
        $this->dataType = $dataType;
    }

    /**
     * @param int $characterMaximumLength
     */
    public function setCharacterMaximumLength(int $characterMaximumLength): void
    {
        $this->characterMaximumLength = $characterMaximumLength;
    }

    /**
     * @param int $characterOctetLength
     */
    public function setCharacterOctetLength(int $characterOctetLength): void
    {
        $this->characterOctetLength = $characterOctetLength;
    }

    /**
     * @param int $numericPrecision
     */
    public function setNumericPrecision(int $numericPrecision): void
    {
        $this->numericPrecision = $numericPrecision;
    }

    /**
     * @param int $numericScale
     */
    public function setNumericScale(int $numericScale): void
    {
        $this->numericScale = $numericScale;
    }

    /**
     * @param int $datetimePrecision
     */
    public function setDatetimePrecision(int $datetimePrecision): void
    {
        $this->datetimePrecision = $datetimePrecision;
    }

    /**
     * @param string $characterSetName
     */
    public function setCharacterSetName(string $characterSetName): void
    {
        $this->characterSetName = $characterSetName;
    }

    /**
     * @param string $collationName
     */
    public function setCollationName(string $collationName): void
    {
        $this->collationName = $collationName;
    }

    /**
     * @param string $columnType
     */
    public function setColumnType(string $columnType): void
    {
        $this->columnType = $columnType;
    }

    /**
     * @param string $columnKey
     */
    public function setColumnKey(string $columnKey): void
    {
        $this->columnKey = $columnKey;
    }

    /**
     * @param string $extra
     */
    public function setExtra(string $extra): void
    {
        $this->extra = $extra;
    }

    /**
     * @param string $columnComment
     */
    public function setColumnComment(string $columnComment): void
    {
        $this->columnComment = $columnComment;
    }


    /**
     * 获取列名称
     * @return string
     */
    public function getColumnName(): string
    {
        return $this->columnName;
    }

    /**
     * 获取列标识号
     * @return int
     */
    public function getOrdinalPosition(): int
    {
        return $this->ordinalPosition;
    }

    /**
     * 获取列的默认值
     * @return mixed
     */
    public function getColumnDefault()
    {
        return $this->columnDefault;
    }

    /**
     * 获取列是否可以为空
     * @return bool
     */
    public function getIsNullable(): bool
    {
        return $this->isNullable;
    }

    /**
     * 获取列数据类型名称
     * @return string
     */
    public function getDataType(): string
    {
        return $this->dataType;
    }

    /**
     * 获取列能保存的最大字符串长度
     * @return int|null
     */
    public function getCharacterMaximumLength(): int
    {
        return $this->characterMaximumLength;
    }

    /**
     * 获取列能保存宽字符的最大长度
     * 如果是utf8mb4情况下为最大字符串长度的4倍
     * @return int|null
     */
    public function getCharacterOctetLength(): int
    {
        return $this->characterOctetLength;
    }

    /**
     * 近似数字数据、精确数字数据、整型数据或货币数据的精度。非数字类型的返回NULL
     * @return int|null
     */
    public function getNumericPrecision(): int
    {
        return $this->numericPrecision;
    }

    /**
     * 近似数字数据、精确数字数据、整数数据或货币数据的小数位数。非数字类型的返回NULL
     * @return int|null
     */
    public function getNumericScale(): int
    {
        return $this->numericScale;
    }

    /**
     * datetime 及 SQL-92 interval 数据类型的子类型代码。其它数据类型返回NULL
     * @return int|null
     */
    public function getDatetimePrecision(): int
    {
        return $this->datetimePrecision;
    }

    /**
     * 如果该列是字符数据或 text 数据类型，那么为字符集返回唯一的名称。否则返回NULL
     * @return string|null
     */
    public function getCharacterSetName(): string
    {
        return $this->characterSetName;
    }

    /**
     * 如果列是字符数据或 text 数据类型，那么为排序次序返回唯一的名称。否则返回NULL
     * @return string|null
     */
    public function getCollationName(): string
    {
        return $this->collationName;
    }

    /**
     * 获取列类型的定义，例如varchar(64)、int(11)、enum('yes','no','notsure')
     * @return string
     */
    public function getColumnType(): string
    {
        return $this->columnType;
    }

    /**
     * 获取列的索引名称('PRI','UNI',...)
     * @return string
     */
    public function getColumnKey(): string
    {
        return $this->columnKey;
    }

    /**
     * 获取额外信息，比如'auto_increment'
     * @return string
     */
    public function getExtra(): string
    {
        return $this->extra;
    }

    /**
     * 获取列备注信息
     * @return string
     */
    public function getColumnComment(): string
    {
        return $this->columnComment;
    }

    /**
     * 根据列的定义初始化增删改查的定义对象
     * @return null
     */
    public function initByDefinition()
    {
        $temp = mackParseArrayKeyToCamelCase($this->originData);
        foreach ($temp as $k => $item)
        {
            if ($k == 'isNullable')
            {
                $item = mackParseYesOrNoToBoolean($item);
            }
            if ($item !== NULL)
            {
                $setter = 'set'.ucfirst($k);
                if (method_exists($this,$setter))
                {
                    $this->$setter($item);
                }
                else
                {
                    $this->$k = $item;
                }
            }
        }
    }

    /**
     * 根据注释，覆盖定义对象的相关属性
     * @return null
     */
    public function initByComment()
    {
        // TODO: Implement initByComment() method.
    }

    /**
     * @return array
     */
    public function getOriginData(): array
    {
        return $this->originData;
    }

    /**
     * @param array $originData
     */
    public function setOriginData(array $originData): void
    {
        $this->originData = $originData;
    }
}
