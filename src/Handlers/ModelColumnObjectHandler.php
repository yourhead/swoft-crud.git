<?php


namespace Swoft\Crud\Handlers;


use Swoft\Crud\Contract\CrudColumnDefinitionObjectInterface;
use Swoft\Crud\Contract\CrudTableDefinitionObjectInterface;
use Swoft\Crud\Contract\MysqlTableColumnObjectHandlerInterface;
use Swoft\Crud\Model\Logic\EntityLogic;
use Swoft\Bean\Annotation\Mapping\Bean;
use Swoft\Bean\Annotation\Mapping\Inject;
use Swoft\Db\Pool;

/**
 * Class ModelColumnObjectHandler
 * @Bean(name="modelHandler")
 */
class ModelColumnObjectHandler implements MysqlTableColumnObjectHandlerInterface
{
    /**
     * @Inject("crudEntityLogic")
     * @var EntityLogic
     */
    private $logic;

    /**
     * @param CrudTableDefinitionObjectInterface $crudTableDefinitionObject
     * @param CrudColumnDefinitionObjectInterface [] $mainTableColumnDefinitions
     * @param CrudColumnDefinitionObjectInterface [] $foreignTableColumnDefinitions
     * @param array $destinationFilePath
     * @return array
     * @example
     *  [
     *      'filePath'  =>  'fileContent'
     *  ]
     */
    public function handle(CrudTableDefinitionObjectInterface $crudTableDefinitionObject, array $mainTableColumnDefinitions, array $foreignTableColumnDefinitions, array $destinationFilePath): array
    {
        $table        = $crudTableDefinitionObject->getTableName();
        $pool         = Pool::DEFAULT_POOL;
        $path         = $destinationFilePath['destinationDirs']['modelDir'];
        $isConfirm    = false;
        $fieldPrefix  = "";
        $tablePrefix  = "";
        $exclude      = "";
        $tplDir       = '@devtool/devtool/resource/template';
        $removePrefix = "";

        $content = $this->logic->create([
            (string)$table,
            (string)$tablePrefix,
            (string)$fieldPrefix,
            (string)$exclude,
            (string)$pool,
            (string)$path,
            (bool)$isConfirm,
            (string)$tplDir,
            (string)$removePrefix,
            $mainTableColumnDefinitions['columnDefinitionObjects'],
        ]);

        return array($destinationFilePath['destinationFiles']['modelFile']=>$content);
    }
}
