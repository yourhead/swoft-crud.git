<?php


namespace Swoft\Crud\Contract;

/**
 * Interface CrudColumnDefinitionObjectInterface
 * @method string getColumnName()
 * @method string getColumnKey()
 * @method string getDataType()
 * @method bool getIsNullable()
 * @method string getColumnType()
 * @method int getCharacterMaximumLength()
 * @method mixed getColumnDefault()
 *
 */
interface CrudColumnDefinitionObjectInterface
{
    /**
     * @return bool
     */
    public function isPrimaryKey():bool;

    public function getLabel(): string;

    public function isShow(): bool;

    public function getViewProperties(): string;

    public function getViewType(): string;

    public function getViewOptions(): array;

    public function getFormItemProperties(): string;

    public function canSearch(): bool;

    public function getSearchType(): string;
}
