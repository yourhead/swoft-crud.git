<?php


namespace Swoft\Crud\Contract;


interface MysqlTableColumnCrudDefinitionInterface
{
    /**
     * 是否展示
     * @return bool
     */
    public function isShow(): bool;

    /**
     * 展示的默认值
     * @return string
     */
    public function getDefaultValue(): string;

    /**
     * 获取列的Label
     * @return string
     */
    public function getLabel(): string;

    /**
     * 获取UI结构字符串
     * @return string
     */
    public function getEleUIText(): string;
}
