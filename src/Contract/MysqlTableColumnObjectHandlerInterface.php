<?php declare(strict_types=1);


namespace Swoft\Crud\Contract;

interface MysqlTableColumnObjectHandlerInterface
{
    /**
     * @param CrudTableDefinitionObjectInterface $crudTableDefinitionObject
     * @param CrudColumnDefinitionObjectInterface [] $mainTableColumnDefinitions
     * @param CrudColumnDefinitionObjectInterface [] $foreignTableColumnDefinitions
     * @param array $destinationFilePath
     * @return array
     * @example
     *  [
     *      'filePath'  =>  'fileContent'
     *  ]
     */
    public function handle(CrudTableDefinitionObjectInterface $crudTableDefinitionObject,Array $mainTableColumnDefinitions,Array $foreignTableColumnDefinitions,Array $destinationFilePath):array;
}
