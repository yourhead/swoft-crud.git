<?php


namespace Swoft\Crud\Contract;


interface MysqlColumnCommentObjectInterface
{
    public function initByCommentString(string $comment);
}
