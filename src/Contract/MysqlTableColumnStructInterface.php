<?php declare(strict_types=1);


namespace Swoft\Crud\Contract;


/**
 * Interface MysqlTableColumn
 * @package App\Console\Command\Crud\Contract
 */
interface MysqlTableColumn
{
    /**
     * 获取列名称
     * @return string
     */
    public function getColumnName(): string;

    /**
     * 获取列标识号
     * @return int
     */
    public function getOrdinalPosition(): int;

    /**
     * 获取列的默认值
     * @return mixed
     */
    public function getColumnDefault();

    /**
     * 获取列是否可以为空
     * @return bool
     */
    public function getIsNullable(): bool;

    /**
     * 获取列数据类型名称
     * @return string
     */
    public function getDataType(): string;

    /**
     * 获取列能保存的最大字符串长度
     * @return int|null
     */
    public function getCharacterMaximumLength(): int;

    /**
     * 获取列能保存宽字符的最大长度
     * 如果是utf8mb4情况下为最大字符串长度的4倍
     * @return int|null
     */
    public function getCharacterOctetLength(): int;

    /**
     * 近似数字数据、精确数字数据、整型数据或货币数据的精度。非数字类型的返回NULL
     * @return int|null
     */
    public function getNumericPrecision():int;

    /**
     * 近似数字数据、精确数字数据、整数数据或货币数据的小数位数。非数字类型的返回NULL
     * @return int|null
     */
    public function getNumericScale():int;

    /**
     * datetime 及 SQL-92 interval 数据类型的子类型代码。其它数据类型返回NULL
     * @return int|null
     */
    public function getDatetimePrecision(): int;

    /**
     * 如果该列是字符数据或 text 数据类型，那么为字符集返回唯一的名称。否则返回NULL
     * @return string|null
     */
    public function getCharacterSetName(): string;

    /**
     * 如果列是字符数据或 text 数据类型，那么为排序次序返回唯一的名称。否则返回NULL
     * @return string|null
     */
    public function getCollationName(): string;

    /**
     * 获取列类型的定义，例如varchar(64)、int(11)、enum('yes','no','notsure')
     * @return string
     */
    public function getColumnType(): string;

    /**
     * 获取列的索引名称('PRI','UNI',...)
     * @return string
     */
    public function getColumnKey(): string;

    /**
     * 获取额外信息，比如'auto_increment'
     * @return string
     */
    public function getExtra(): string;

    /**
     * 获取列备注信息
     * @return string
     */
    public function getColumnComment(): string;

    /**
     * 根据列的定义初始化增删改查的定义对象
     * @return null
     */
    public function initByDefinition();

    /**
     * 根据注释，覆盖定义对象的相关属性
     * @return null
     */
    public function initByComment();
}
